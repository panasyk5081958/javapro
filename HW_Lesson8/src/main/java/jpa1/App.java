package jpa1;

import javax.persistence.*;
import java.util.*;

public class App {
    static EntityManagerFactory emf;
    static EntityManager em;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            emf = Persistence.createEntityManagerFactory("JPAMenu");
            em = emf.createEntityManager();
            try {
                while (true) {
                    System.out.println("1: add dish");
                    System.out.println("2: add random dishes");
                    System.out.println("3: delete dish");
                    System.out.println("4: change dish");
                    System.out.println("5: view dishes");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
                        case "1":
                            addDish(sc);
                            break;
                        case "2":
                            insertRandomDishes(sc);
                            break;
                        case "3":
                            deleteDish(sc);
                            break;
                        case "4":
                            changeDishPrice(sc);
                            break;
                        case "5":
                            viewDishes();
                            break;
                        default:
                            return;
                    }
                }
            } finally {
                sc.close();
                em.close();
                emf.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }

    private static void addDish(Scanner sc) {
        System.out.print("Enter dish name: ");
        String dishName = sc.nextLine();
        System.out.print("Enter dish price: ");
        String sPrice = sc.nextLine();
        double price = Integer.parseInt(sPrice);
        System.out.print("Enter dish weight: ");
        String sWeight = sc.nextLine();
        double weight = Integer.parseInt(sWeight);
        System.out.print("Enter availability of a discount (true or false): ");
        String sDiscount = sc.nextLine();
        boolean discount = Boolean.parseBoolean(sDiscount);

        em.getTransaction().begin();
        try {
            Dish d = new Dish (dishName, price, weight, discount);
            em.persist(d);
            em.getTransaction().commit();

            System.out.println(d.getId());
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void deleteDish(Scanner sc) {
        System.out.print("Enter dish id: ");
        String sId = sc.nextLine();
        long id = Long.parseLong(sId);

        Dish d = em.getReference(Dish.class, id);
        if (d == null) {
            System.out.println("Dish not found!");
            return;
        }

        em.getTransaction().begin();
        try {
            em.remove(d);
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void changeDishPrice(Scanner sc) {
        System.out.print("Enter dish name: ");
        String dishName = sc.nextLine();

        System.out.print("Enter new price: ");
        String sPrice = sc.nextLine();
        double price = Integer.parseInt(sPrice);

        Dish d = null;
        try {
            Query query = em.createQuery("SELECT x FROM Dish x WHERE x.dishName = :dishName", Dish.class);
            query.setParameter("dishName", dishName);

            d = (Dish) query.getSingleResult();
        } catch (NoResultException ex) {
            System.out.println("Dish not found!");
            return;
        } catch (NonUniqueResultException ex) {
            System.out.println("Non unique result!");
            return;
        }

        em.getTransaction().begin();
        try {
            d.setPrice(price);
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }

    private static void insertRandomDishes(Scanner sc) {
        System.out.print("Enter dishes count: ");
        String sCount = sc.nextLine();
        int count = Integer.parseInt(sCount);

        em.getTransaction().begin();
        try {
            for (int i = 0; i < count; i++) {
                Dish d = new Dish(randomName(), RND.nextDouble(100), RND.nextDouble(200), RND.nextBoolean());
                em.persist(d);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }
    private static Dish randomDishWeight(){
        Query query = em.createQuery("SELECT d FROM Dish d", Dish.class);
        List<Dish> list = (List<Dish>) query.getResultList();
        Dish[] arr = list.toArray(new Dish[0]);
        return arr[RND.nextInt(arr.length)];
    }


    private static void viewDishes() {
        Query query = em.createQuery("SELECT d FROM Dish d", Dish.class);
        List<Dish> list = (List<Dish>) query.getResultList();

        for (Dish d : list)
            System.out.println(d);
    }

    private static void chooseByPrice (Scanner sc) {
        System.out.println("Enter the lowest dish price");
        String lPrice = sc.nextLine();
        double lowPrice = Integer.parseInt(lPrice);
        System.out.println("Enter the highest dish price");
        String hPrice = sc.nextLine();
        double highPrice = Integer.parseInt(hPrice);

        Query queryPrice = em.createQuery("SELECT d FROM Dish d WHERE d.price >= : lowPrice AND d.price <= : highPrice", Dish.class);
        List<Dish> list1 = (List<Dish>) queryPrice.getResultList();

        for (Dish d : list1)
            System.out.println(d);
    }

    private static void chooseByDiscount () {
        Query queryDiscount  = em.createQuery("SELECT d FROM Dish d WHERE d.discount  = true", Dish.class);
        List<Dish> list2 = (List<Dish>) queryDiscount .getResultList();

        for (Dish d : list2)
            System.out.println(d);

    }

    static final String[] NAMES = {"salad", "borsch", "vareniki", "kvass", "donuts"};
    static final Random RND = new Random();

    static String randomName() {
        return NAMES[RND.nextInt(NAMES.length)];
    }

    private static void chooseByWeight () {
        List<Dish> list3 = new ArrayList<>();
        double sumWeight = 0;
        do {
            Dish r = randomDishWeight();
            sumWeight += r.getWeight();
            list3.add(r);
        } while (sumWeight <= 1000);
        for (Dish r : list3) {
            System.out.println(r);
        }
    }
}



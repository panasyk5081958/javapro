package com.example.hw_lesson2;

import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

@WebServlet (name = "SurveyServlet", value = "/survey")
public class SurveyServlet extends HttpServlet {
    private static AtomicInteger javaN = new AtomicInteger(0);
    private static AtomicInteger pytonN = new AtomicInteger(0);
    private static AtomicInteger eclipseN = new AtomicInteger(0);
    private static AtomicInteger ideaN = new AtomicInteger(0);
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String language = request.getParameter("language");
        String program = request.getParameter("program");

        if (!language.equals(null) && !program.equals(null) ) {

            if (language.equals("java")) {
                javaN.getAndIncrement();
            } else if (language.equals("pyton")) {
                pytonN.getAndIncrement();
            }
            if (program.equals("eclipse")) {
                eclipseN.getAndIncrement();
            } else if (program.equals("idea")) {
                ideaN.getAndIncrement();
            }
            HttpSession session = request.getSession(true);

            session.setAttribute("language", language);
            session.setAttribute("program", program);
            session.setAttribute("java", javaN);
            session.setAttribute("pyton", pytonN);
            session.setAttribute("eclipse", eclipseN);
            session.setAttribute("idea", ideaN);

            response.sendRedirect("index.jsp");
        }
    }
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String a = request.getParameter("a");
        HttpSession session = request.getSession(false);
        if ("exit".equals(a) && session != null) {
            session.removeAttribute("language");
            session.removeAttribute("program");
        }
        response.sendRedirect("index.jsp");
    }
}

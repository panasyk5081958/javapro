<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ page import="java.util.concurrent.atomic.AtomicInteger" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Lesson_2 Forms</title>
    </head>
    <body>
        <h1>Information</h1>
        <% String language = (String)session.getAttribute("language");
        String program = (String)session.getAttribute("program");
        AtomicInteger javaN =  (AtomicInteger) session.getAttribute("javaN");
        AtomicInteger pytonN =  (AtomicInteger) session.getAttribute("pytonN");
        AtomicInteger eclipseN =  (AtomicInteger) session.getAttribute("eclipseN");
        AtomicInteger ideaN =  (AtomicInteger) session.getAttribute("ideaN"); %>


        <% if (language == null || "".equals(language) && program == null || "".equals(program)) { %>
        <div>
            <form action="/survey" method="POST">
                <h2>1.What language do you prefer?</h2>
                <p><label><input type="radio" name="language" value="java">Java</label></p>
                <p><label><input type="radio" name="language" value="pyton">Pyton</label></p>
                <br>
                <h2>2.What development environment do you prefer?</h2>
                <p><label><input type="radio" name="program" value="eclipse">Eclipse</label></p>
                <p><label><input type="radio" name="program" value="idea">IntelliJ Idea</label></p>
                <br>
                <input type="submit" value="Submit">
            </form>
        </div>
        <%} else { %>
        <div>
            <h1>Counter of results</h1>
            <table width="600px" border="2px">
                <caption>
                    <th width="60%">Question</th>
                    <th width="20%">Сhoice 1</th>
                    <th width="20%">Сhoice 2</th>
                </caption>
                <tr>
                    <th width="60%">1.What language do you prefer?</th>
                    <td width="20%" align="center"><%= javaN %></td>
                    <td width="20%" align="center"><%= pytonN %></td>
                </tr>
                <tr>
                    <th width="60%">2.What language do you prefer?</th>
                    <td width="20%" align="center"><%= eclipseN %></td>
                    <td width="20%" align="center"><%= ideaN %></td>
                </tr>
            </table>
            <br><br>
            Click this link to <a href="/survey?a=exit"><h3>Restart</h3></a>
        </div>
        <% } %>
    </body>
</html>
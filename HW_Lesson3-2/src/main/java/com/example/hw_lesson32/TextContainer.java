package com.example.hw_lesson32;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

@SaveTo(path = "D:\\annotation_test.txt")
public class TextContainer {
    String text = "Annotation test";
    @SaveMethod
    public void save (File file, String text) {
        try
            (FileWriter fw = new FileWriter(file)){
            fw.write(text);
            fw.close();
            System.out.println("The text " + "'"   + text + "'" + " saved to file: " + "'" + file + "'");
        } catch (IOException e)   {
            throw new RuntimeException();
        }

    }

    public String getText() {
        return text;
    }
}

package com.example.hw_lesson32;
import java.lang.annotation.*;

@Inherited
@Target(value = ElementType.TYPE)
@Retention(value = RetentionPolicy.RUNTIME)

public @interface SaveTo {
    String path ();
}

package com.example.hw_lesson32;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Method;
import java.lang.NoSuchMethodException;
import java.lang.IllegalAccessException;
import java.lang.reflect.InvocationTargetException;

public class Main {
    public static void main (String[] args) {
            final Class<?> cls = TextContainer.class;
            SaveTo an = cls.getAnnotation(SaveTo.class);
            String path = an.path();
            Method[] methods = cls.getMethods();
            for (Method method : methods) {
                if (method.isAnnotationPresent(SaveMethod.class)) {
                    File file = new File(path);
                    TextContainer container = new TextContainer();
                    try {
                        method.invoke (container, file, container.getText());
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
    }
}

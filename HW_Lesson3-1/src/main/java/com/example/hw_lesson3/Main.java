package com.example.hw_lesson3;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Main {
    public static void main (String [] args) {

        try {
            MyTest myTest = new MyTest();
            Method method = MyTest.class.getDeclaredMethod("methodAnnotation", int.class, int.class);
            MyAnnotation annotation = method.getAnnotation(MyAnnotation.class);
            method.invoke(myTest, annotation.param1(), annotation.param2());
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e)  {
            throw new RuntimeException();
        }
    }
}

$(document).ready(function() {
    $.getJSON('/rate', function(data) {
        $('#rate_uah').text(data.rates.UAH);
        $('#date_uah').text(data.date);
        $('#rate_usd').text(data.rates.USD);
        $('#date_usd').text(data.date);
    });
});
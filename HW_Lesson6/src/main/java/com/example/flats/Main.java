package com.example.flats;

import java.sql.*;
import java.util.Random;
import java.util.Scanner;

public class Main {
    static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/flats?serverTimezone=Europe/Kiev";
    static final String DB_USER = "root";
    static final String DB_PASSWORD = "password";

    static Connection conn;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        try {
            try {
                conn = DriverManager.getConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
                initDB();
                while (true) {

                    System.out.println("1: add flat");
                    System.out.println("2: add random flats");
                    System.out.println("3: delete flat");
                    System.out.println("4: change flat");
                    System.out.println("5: view flats");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
                        case "1":
                            addFlat(sc);
                            break;
                        case "2":
                            insertRandomFlats(sc);
                            break;
                        case "3":
                            deleteFlat(sc);
                            break;
                        case "4":
                            changeFlat(sc);
                            break;
                        case "5":
                            viewFlats();
                            break;
                        default:
                            return;
                    }
                }

            } finally {
                sc.close();
                if (conn != null) conn.close();
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return;
        }

    }
    private static void initDB() throws SQLException {
        Statement st = conn.createStatement();
        try {
            st.execute("DROP TABLE IF EXISTS Flats");
            st.execute("CREATE TABLE Flats (code INT NOT NULL " +
                    "AUTO_INCREMENT PRIMARY KEY, city VARCHAR(30) " +
                    "NOT NULL, district VARCHAR(50), address VARCHAR(128), " +
                    "rooms INT, area INT, price INT)");
        } finally {
            st.close();
        }
    }
    private static void addFlat (Scanner sc) throws SQLException {
        System.out.print("Enter city where's flat: ");
        String city = sc.nextLine();
        System.out.print("Enter district where's flat: ");
        String distr = sc.nextLine();
        System.out.print("Enter address of the flat: ");
        String addr = sc.nextLine();
        System.out.print("Enter apartment area: ");
        String sArea = sc.nextLine();
        int area = Integer.parseInt(sArea);
        System.out.print("Enter number of rooms: ");
        String sRooms = sc.nextLine();
        int rooms = Integer.parseInt(sRooms);
        System.out.print("Enter desired price: ");
        String sPrice = sc.nextLine();
        int price = Integer.parseInt(sPrice);

        PreparedStatement ps = conn.prepareStatement("INSERT INTO Flats (city, distr, addr, area, rooms, price) VALUES(?, ?, ?, ?, ?, ?)");

        try {
            ps.setString(1, "Kyiv");
            ps.setString(2, "Obolon");
            ps.setString(3, "prospekt GD");
            ps.setInt(4, 50);
            ps.setInt(5, 2);
            ps.setInt(6, 30000);
            ps.executeUpdate();

            ps.setString(1, "Bila Tserkva");
            ps.setString(2, "Center");
            ps.setString(3, "prospekt YM");
            ps.setInt(4, 30);
            ps.setInt(5, 1);
            ps.setInt(6, 20000);
            ps.executeUpdate();

        } finally {
            ps.close();
        }
    }
    private static void deleteFlat (Scanner sc) throws SQLException {
        System.out.print("Enter city where's flat: ");
        String name = sc.nextLine();

        PreparedStatement ps = conn.prepareStatement("DELETE FROM Flats WHERE city = ?");
        try {
            ps.setString(1, "Odesa");
            ps.executeUpdate();
        } finally {
            ps.close();
        }
    }
    private static void changeFlat(Scanner sc) throws SQLException {
        System.out.print("Enter city where's flat: ");
        String name = sc.nextLine();
        System.out.print("Enter new price: ");
        String sPrice = sc.nextLine();
        int price = Integer.parseInt(sPrice);

        PreparedStatement ps = conn.prepareStatement("UPDATE Flats SET price = ? WHERE city = ?");
        try {
            ps.setInt(1, price);
            ps.setString(2, "city");
            ps.executeUpdate();
        } finally {
            ps.close();
        }
    }
    private static void insertRandomFlats(Scanner sc) throws SQLException {
        System.out.print("Enter flats count: ");
        String sCount = sc.nextLine();
        int count = Integer.parseInt(sCount);
        Random rnd = new Random();

        conn.setAutoCommit(false);
        try {
            try {
                PreparedStatement ps = conn.prepareStatement("INSERT INTO Flats (city, price) VALUES(?, ?)");
                try {
                    for (int i = 0; i < count; i++) {
                        ps.setString(1, "City" + i);
                        ps.setInt(2, rnd.nextInt(100));
                        ps.executeUpdate();
                    }
                    conn.commit();
                } finally {
                    ps.close();
                }
            } catch (Exception ex) {
                conn.rollback();
            }
        } finally {
            conn.setAutoCommit(true);
        }
    }
    private static void viewFlats() throws SQLException {
        PreparedStatement ps = conn.prepareStatement("SELECT * FROM Flats");
        try {
            ResultSet rs = ps.executeQuery();

            try {
                ResultSetMetaData md = rs.getMetaData();

                for (int i = 1; i <= md.getColumnCount(); i++)
                    System.out.print(md.getColumnName(i) + "\t\t");
                System.out.println();

                while (rs.next()) {
                    for (int i = 1; i <= md.getColumnCount(); i++) {
                        System.out.print(rs.getString(i) + "\t\t");
                    }
                    System.out.println();
                }
            } finally {
                rs.close();
            }
        } finally {
            ps.close();
        }
    }
}

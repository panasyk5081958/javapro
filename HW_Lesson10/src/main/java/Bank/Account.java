package Bank;

import javax.persistence.*;

@Entity
@Table(name = "Accounts")

public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="Id_account")
    private long id;

    @OneToOne
    @JoinColumn(name = "User_ID")
    private User user;

    @Column(name = "Account_USD")
    private long accUSD;

    @Column(name = "Sum_USD")
    private double USD;

    @Column(name = "Account_EUR")
    private long accEUR;

    @Column(name = "Sum_EUR")
    private double EUR;

    @Column(name = "Account_UAH")
    private long accUAH;

    @Column(name = "Sum_UAH")
    private double UAH;

    @Column(name = "Sum_total_UAH")
    private long totalUAH;

    public Account(User user, long accUSD, double USD, long accEUR, double EUR, long accUAH, double UAH, long totalUAH) {
        this.user = user;
        this.accUSD = accUSD;
        this.USD = USD;
        this.accEUR = accEUR;
        this.EUR = EUR;
        this.accUAH = accUAH;
        this.UAH = UAH;
        this.totalUAH = totalUAH;
    }

    public Account () {};

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getAccUSD() {
        return accUSD;
    }

    public void setAccUSD(long accUSD) {
        this.accUSD = accUSD;
    }

    public double getUSD() {
        return USD;
    }

    public void setUSD(double USD) {
        this.USD = USD;
    }

    public long getAccEUR() {
        return accEUR;
    }

    public void setAccEUR(long accEUR) {
        this.accEUR = accEUR;
    }

    public double getEUR() {
        return EUR;
    }

    public void setEUR(double EUR) {
        this.EUR = EUR;
    }

    public long getAccUAH() {
        return accUAH;
    }

    public void setAccUAH(long accUAH) {
        this.accUAH = accUAH;
    }

    public double getUAH() {
        return UAH;
    }

    public void setUAH(double UAH) {
        this.UAH = UAH;
    }

    public long getTotalUAH() {
        return totalUAH;
    }

    public void setTotalUAH(long totalUAH) {
        this.totalUAH = totalUAH;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", user=" + user +
                ", accUSD=" + accUSD +
                ", USD=" + USD +
                ", accEUR=" + accEUR +
                ", EUR=" + EUR +
                ", accUAH=" + accUAH +
                ", UAH=" + UAH +
                ", totalUAH=" + totalUAH +
                '}';

    }
}


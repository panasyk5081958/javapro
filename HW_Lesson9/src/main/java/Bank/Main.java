package Bank;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.Scanner;

public class Main {
    static EntityManagerFactory emf;
    static EntityManager em;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        try {
            emf = Persistence.createEntityManagerFactory("Bank");
            em = emf.createEntityManager();

            try {
                while (true) {
                    System.out.println("1: add user");
                    System.out.println("2: view accounts");
                    System.out.println("3: top up the account");
                    System.out.println("4: add transaction");
                    System.out.println("5: exchange currency");
                    System.out.println("6: get total amount in hryvnia");
                    System.out.print("-> ");

                    String s = sc.nextLine();
                    switch (s) {
                        case "1":
                            addUser(sc);
                            break;
                        case "2":
                            viewAccounts();
                            break;
                        case "3":
                            topUpAccount(sc);
                            break;
                        case "4":
                            addTransaction(sc);
                            break;
                        case "5":
                            currencyExchange(sc);
                            break;
                        case "6":
                            getTotalUAH(sc);
                            break;
                        default:
                            return;
                    }
                }
            } finally {
                sc.close();
                em.close();
                emf.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        }
    }

    private static void addUser(Scanner sc) {
        System.out.print("Enter user first name: ");
        String firstName = sc.nextLine();
        System.out.print("Enter user last name: ");
        String lastName = sc.nextLine();

        em.getTransaction().begin();
        try {
            User user = new User(firstName, lastName);
            em.persist(user);
            em.getTransaction().commit();

            System.out.println(user.getId());
        } catch (Exception ex) {
            em.getTransaction().rollback();
        }
    }
    private static void viewAccounts() {
        Query query = em.createQuery("SELECT acc FROM Account acc", Account.class);
        List<Account> list = (List<Account>) query.getResultList();

        for (Account acc : list)
            System.out.println(acc);
    }
    private static void topUpAccount(Scanner sc) {
        System.out.print("Enter Id_account to top up: ");
        String sIdAccount = sc.nextLine();
        long idAccount = Long.parseLong(sIdAccount);

        Account account = em.find(Account.class, idAccount);

        if (account == null) {
            System.out.println("Account does not exist");
            return;
        }

        System.out.println("Enter the currency to top up the account (USD, EUR, UAH): ");
        String sCurrency = sc.nextLine();
        CurrencyType currency = null;

        try {
            currency = CurrencyType.valueOf(sCurrency);
        } catch (IllegalArgumentException e) {
            System.out.println("Сhoose the right currency");
            return;
        }

        System.out.print("Enter the amount to replenish the account: ");
        String sAmount = sc.nextLine();
        double amountReplenish = Double.parseDouble(sAmount);

        em.getTransaction().begin();
        try {
            switch (currency) {
                case USD:
                    account.setUSD(account.getUSD() + amountReplenish);
                    break;
                case EUR:
                    account.setEUR(account.getEUR() + amountReplenish);
                    break;
                case UAH:
                    account.setUAH(account.getUAH() + amountReplenish);
                    break;
                default:
                    System.out.println("Recharge amount error");
                    return;
            }
            em.getTransaction().commit();
            System.out.println("Account replenished successfully");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("Account replenishment error");
        }
    }
    private static void addTransaction(Scanner sc) {
        System.out.print("Enter the sender ID_account: ");
        String sSenderId = sc.nextLine();
        long senderId = Long.parseLong(sSenderId);

        System.out.print("Enter the receiver ID_account: ");
        String sReceiverId = sc.nextLine();
        long receiverId = Long.parseLong(sReceiverId);

        System.out.print("Enter the transaction amount: ");
        String sAmount = sc.nextLine();
        double amount = Double.parseDouble(sAmount);

        Account senderAccount = em.find(Account.class, senderId);
        Account receiverAccount = em.find(Account.class, receiverId);

        if (senderAccount == null || receiverAccount == null) {
            System.out.println("Account error");
            return;
        }

        System.out.println("Enter the transaction currency (USD, EUR, UAH): ");
        String currencyStr = sc.nextLine();
        CurrencyType currency = null;

        try {
            currency = CurrencyType.valueOf(currencyStr);
        } catch (IllegalArgumentException e) {
            System.out.println("Сhoose the right currency");
            return;
        }

        double senderBalance = 0.0;
        double receiverBalance = 0.0;

        switch (currency) {
            case USD:
                senderBalance = senderAccount.getUSD();
                receiverBalance = receiverAccount.getUSD();
                break;
            case EUR:
                senderBalance = senderAccount.getEUR();
                receiverBalance = receiverAccount.getEUR();
                break;
            case UAH:
                senderBalance = senderAccount.getUAH();
                receiverBalance = receiverAccount.getUAH();
                break;
            default:
                System.out.println("Invalid currency selection");
                return;
        }

        if (senderBalance < amount) {
            System.out.println("Transaction is not possible: there is not enough money on the sender's account");
            return;
        }

        em.getTransaction().begin();
        try {
            switch (currency) {
                case USD:
                    senderAccount.setUSD(senderAccount.getUSD() - amount);
                    receiverAccount.setUSD(receiverAccount.getUSD() + amount);
                    break;
                case EUR:
                    senderAccount.setEUR(senderAccount.getEUR() - amount);
                    receiverAccount.setEUR(receiverAccount.getEUR() + amount);
                    break;
                case UAH:
                    senderAccount.setUAH(senderAccount.getUAH() - amount);
                    receiverAccount.setUAH(receiverAccount.getUAH() + amount);
                    break;
                default:
                    break;
            }

            java.util.Date utilDate = new java.util.Date();
            java.sql.Date sqlDate = new java.sql.Date(utilDate.getTime());

            Transaction transaction = new Transaction(receiverAccount.getUser(), senderAccount.getUser(),  currency.name(), amount,  sqlDate);

            em.persist(transaction);

            em.getTransaction().commit();

            System.out.println("Transaction completed successfully");
        } catch (Exception ex) {
            em.getTransaction().rollback();
            System.out.println("Error of transaction");
        }
    }
    private static void currencyExchange(Scanner sc) {
        System.out.print("Enter user ID_account: ");
        long userId = Long.parseLong(sc.nextLine());

        try {
            em.getTransaction().begin();

            User user = em.find(User.class, userId);

            if (user == null) {
                System.out.println("User not found");
                return;
            }

            Account account = user.getAccount();

            if (account == null) {
                System.out.println("User's account not found");
                return;
            }

            System.out.print("Enter type of original currency (USD, EUR, UAH): ");
            String originalCurrencyStr = sc.nextLine();
            CurrencyType sourceCurrency = null;

            try {
                sourceCurrency = CurrencyType.valueOf(originalCurrencyStr);
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid type of original currency");
                return;
            }

            System.out.print("Enter target currency (USD, EUR, UAH): ");
            String targetCurrencyStr = sc.nextLine();
            CurrencyType targetCurrency = null;

            try {
                targetCurrency = CurrencyType.valueOf(targetCurrencyStr);
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid target currency");
                return;
            }

            System.out.print("Enter conversion rate (1 " + sourceCurrency + " = ? " + targetCurrency + "): ");
            double conversionRate = Double.parseDouble(sc.nextLine());

            System.out.print("Enter amount to exchange: ");
            double amountToExchange = Double.parseDouble(sc.nextLine());

            double convertedAmount = amountToExchange * conversionRate;

            switch (sourceCurrency) {

                case USD:
                    account.setUSD(account.getUSD() - amountToExchange);
                    break;
                case EUR:
                    account.setEUR(account.getEUR() - amountToExchange);
                    break;
                case UAH:
                    account.setUAH(account.getUAH() - amountToExchange);
                    break;
                default:

                    break;
            }

            switch (targetCurrency) {

                case USD:
                    account.setUSD(account.getUSD() + convertedAmount);
                    break;
                case EUR:
                    account.setEUR(account.getEUR() + convertedAmount);
                    break;
                case UAH:
                    account.setUAH(account.getUAH() + convertedAmount);
                    break;
                default:
                    break;
            }

            em.getTransaction().commit();
            System.out.println("Exchange of currency completed successfully");
        } catch (Exception ex) {

            em.getTransaction().rollback();
            System.out.println("Exchange of currency failed");
        }
    }

    private static void getTotalUAH(Scanner sc) {
        System.out.print("Enter user ID_account: ");
        long userId = Long.parseLong(sc.nextLine());


        User user = em.find(User.class, userId);

        if (user == null) {
            System.out.println("User not found");
            return;
        }

        double balanceInUAH = 0.0;


        Account account = user.getAccount();
        balanceInUAH = account.getUAH() + (account.getUSD() * ExchangeRate.getSellingRateUSD()) + (account.getEUR() * ExchangeRate.getSellingRateEUR());

        System.out.println("Total balance in UAH for user ID " + userId + ": " + balanceInUAH);
    }





}

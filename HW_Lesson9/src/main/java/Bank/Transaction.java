package Bank;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table (name = "Transactions")
public class Transaction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="Id_transaction")
    private long id;

    @ManyToOne
    @JoinColumn(name = "Receiver")
    private User paymentReceiver;

    @ManyToOne
    @JoinColumn(name = "Sender")
    private User paymentSender;

    private String currency;

    private double amount;

    private java.sql.Date date;

    public Transaction(User paymentReceiver, User paymentSender, String currency, double amount, Date date) {
        this.paymentReceiver = paymentReceiver;
        this.paymentSender = paymentSender;
        this.currency = currency;
        this.amount = amount;
        this.date = date;
    }

    public Transaction() {}

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getPaymentReceiver() {
        return paymentReceiver;
    }

    public void setPaymentReceiver(User paymentReceiver) {
        this.paymentReceiver = paymentReceiver;
    }

    public User getPaymentSender() {
        return paymentSender;
    }

    public void setPaymentSender(User paymentSender) {
        this.paymentSender = paymentSender;
    }
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", paymentReceiver=" + paymentReceiver +
                ", paymentSender=" + paymentSender +
                ", currency='" + currency + '\'' +
                ", amount=" + amount +
                ", date=" + date +
                '}';
    }
}

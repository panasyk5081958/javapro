package Bank;

import javax.persistence.*;
import java.sql.Date;

@Entity
@Table(name = "Exchange_rates")

public class ExchangeRate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="Id_exchange_operation")
    private long id;

    @Column(name = "Purchase rate_USD")
    private static double purchaseRateUSD;

    @Column(name = "Selling rate_USD")
    private static double sellingRateUSD;

    @Column(name = "Purchase rate_EUR")
    private static double purchaseRateEUR;

    @Column(name = "Selling rate_EUR")
    private static double sellingRateEUR;

    @Column(name = "Rate_date")
    private Date date;

    public ExchangeRate(double purchaseRateUSD, double sellingRateUSD, double purchaseRateEUR, double sellingRateEUR, Date rateDate) {
        this.purchaseRateUSD = purchaseRateUSD;
        this.sellingRateUSD = sellingRateUSD;
        this.purchaseRateEUR = purchaseRateEUR;
        this.sellingRateEUR = sellingRateEUR;
        this.date = date;
    }

    public ExchangeRate () {};

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public static double getPurchaseRateUSD() {
        return purchaseRateUSD;
    }

    public void setPurchaseRateUSD(double purchaseRateUSD) {
        this.purchaseRateUSD = purchaseRateUSD;
    }

    public static double getSellingRateUSD() {
        return sellingRateUSD;
    }

    public void setSellingRateUSD(double sellingRateUSD) {
        this.sellingRateUSD = sellingRateUSD;
    }

    public static double getPurchaseRateEUR() {
        return purchaseRateEUR;
    }

    public void setPurchaseRateEUR(double purchaseRateEUR) {
        this.purchaseRateEUR = purchaseRateEUR;
    }

    public static double getSellingRateEUR() {
        return sellingRateEUR;
    }

    public void setSellingRateEUR(double sellingRateEUR) {
        this.sellingRateEUR = sellingRateEUR;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "ExchangeRate{" +
                "id=" + id +
                ", purchaseRateUSD=" + purchaseRateUSD +
                ", sellingRateUSD=" + sellingRateUSD +
                ", purchaseRateEUR=" + purchaseRateEUR +
                ", sellingRateEUR=" + sellingRateEUR +
                ", rateDate=" + date +
                '}';
    }
}

package academy.prog.chatserversprint.model;

import lombok.Data;

@Data
public class FileDTO {
    private long id;
    private String fileName;
    private String fileData;
    private byte[] fileDataByte;
}
